#![allow(non_snake_case)]

use std::env;
use std::process;

use minigrep::Config;

fn main()
{
	let args : Vec<String> = env::args().collect();
	
	let config = Config::new(&args).unwrap_or_else(
		|err| {
			eprintln!("Un problème est survenu lors du traitement des arguments : {}", err);
			
			process::exit(1);
		}
	);
	
	if let Err(e) = minigrep::run(config)
	{
		eprintln!("Erreur de l'application : {}", e);
		
		process::exit(1);
	}
}