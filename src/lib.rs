#![allow(non_snake_case)]

use std::env;
use std::fs;
use std::error::Error;

pub struct Config
{
	pub requete : std::string::String,
	pub nomFichier : std::string::String,
	pub casseSensible : bool,
}

impl Config
{
	pub fn new(args : &[String]) -> Result<Config, &str>
	{
		if args.len() < 3
		{
			return Err("pas suffisamment d'arguments");
		}
		
		let requete = args[1].clone();
		let nomFichier = args[2].clone();
		
		let casseSensible = env::var("CASE_INSENSITIVE").is_err();
		
		Ok(Config { requete, nomFichier, casseSensible })
	}
}

pub fn run(config : Config) -> Result<(), Box<dyn Error>>
{
	let contenu = fs::read_to_string(config.nomFichier)?;
	
	let resultats = if config.casseSensible
	{
		recherche(&config.requete, &contenu)
	}
	else
	{
		rechercheCasseInsensible(&config.requete, &contenu)
	};
	
	for ligne in resultats
	{
		println!("{}", ligne);
	}
	
	Ok(())
}

pub fn recherche<'a>(requete : &str, contenu : &'a str) -> Vec<&'a str>
{
	let mut resultats = Vec::new();
	
	for ligne in contenu.lines()
	{
		if ligne.contains(requete)
		{
			resultats.push(ligne);
		}
	}
	
	resultats
}

pub fn rechercheCasseInsensible<'a>(requete : &str, contenu : &'a str) -> Vec<&'a str>
{
	let requete = requete.to_lowercase();
	let mut resultats = Vec::new();
	
	for ligne in contenu.lines()
	{
		if ligne.to_lowercase().contains(&requete)
		{
			resultats.push(ligne);
		}
	}
	
	resultats
}

// Tests.

#[cfg(test)]
mod tests
{
	use super::*;
	
	#[test]
	fn casseSensible()
	{
		let requete = "duct";
		let contenu = "\
Rust:
safe, fast, productive.
Pick three.
Duct tape.";
		
		assert_eq!(vec!["safe, fast, productive."], recherche(requete, contenu));
	}
	
	#[test]
	fn casseInsensible()
	{
		let requete = "rUsT";
		let contenu = "\
Rust:
safe, fast, productive.
Pick three.
Trust me.";
		
		assert_eq!(
			vec!["Rust:", "Trust me."],
			rechercheCasseInsensible(requete, contenu)
		);
	}
}