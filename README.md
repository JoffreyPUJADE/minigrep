# MiniGREP

Création d'un outil GREP en Rust, selon le chapitre 12 du book de Rust.

# Sommaire

* [Projet](#projet)
* [Utilisation](#utilisation)

# Projet

GREP est un outil sous Unix permettant la recherche de regex dans un fichier.
Le [chapitre 12](https://doc.rust-lang.org/book/ch12-00-an-io-project.html) du [book de Rust](https://doc.rust-lang.org/book/title-page.html) propose la création d'une version
simplifiée de GREP, afin de simplifier l'apprentissage du langage.

# Utilisation

L'utilisation du projet pourra se faire de la manière suivante :
`[exécutable] [chaîneÀRechercher] [nomDuFichier]`.